## Description
Precor Connect spiff entitlement service SDK for javascript.

## Features

##### Update partnerRep
* [documentation](features/UpdatePartnerRep.feature)


## Setup

**install via jspm**  
```shell
jspm install spiff-entitlement-service-sdk=bitbucket:precorconnect/spiff-entitlement-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import SpiffEntitlementServiceSdk,{SpiffEntitlementServiceSdkConfig} from 'spiff-entitlement-service-sdk'

const spiffEntitlementServiceSdkConfig = 
    new SpiffEntitlementServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const spiffEntitlementServiceSdk = 
    new SpiffEntitlementServiceSdk(
        spiffEntitlementServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```