/**
 * @module
 * @description product group service sdk public API
 */
export {default as SpiffEntitlementServiceSdkConfig} from './spiffEntitlementServiceSdkConfig';
export {default as default} from './spiffEntitlementServiceSdk';
