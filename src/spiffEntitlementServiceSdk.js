import SpiffEntitlementServiceSdkConfig from './spiffEntitlementServiceSdkConfig';
import DiContainer from './diContainer';
import UpdatePartnerRepFeature from './updatePartnerRepFeature';

/**
 * @class {SpiffEntitlementServiceSdk}
 */
export default class SpiffEntitlementServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {SpiffEntitlementServiceSdkConfig} config
     */
    constructor(config:SpiffEntitlementServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * Update the partner rep
     * @param {number} partnerSaleRegistrationId
     * @param {string} partnerRepUserId
     * @param {string} accessToken
     * @returns {Promise.<void>}
     */
    updatePartnerRep(partnerSaleRegistrationId:number,
                     partnerRepUserId:string,
                     accessToken:string):Promise<void> {

        this
            ._diContainer
            .get(UpdatePartnerRepFeature)
            .execute(
                partnerSaleRegistrationId,
                partnerRepUserId,
                accessToken
            );

    }


}
